# CH37x BIOS
A standalone, bootable BIOS for use with CH375/376 modules.

Specific Targets:
* Onboard CH376 module of the [Homebrew 8088](http://www.homebrew8088.com) mainboards
* Generic "ISA to USB" cards using CH375 chips

## Configuration
The disc.asm file contains a few configurable options at the top of the file.  These are summarized as follows:

MAX_CYL, MAX_HPC, and MAX_SPT define the largest geometry that is supported.  The default is the common 504Mb "1024 cylinders, 16 heads, 63 sectors" geometry. Even if you use a 64Gb drive, you'll only see 504Mb.  You can experiment with a MAX_HPC value of 255 to present an 8Gb geometry, but this may have more compatibility issues.

DISPLAY_CH376S_ERRORS:  If enabled, you'll see more verbose errors if the CH375/6 responds unpredictably.  Errors are reported in the form "CH37X ERROR/FUNC: aa/bb", where aa is the status the CH375/6 reported, and bb is the BIOS operation number.  Mostly useful for debugging.

INJECT_INT19_HANDLER:  A mini-bootloader will be registered on interrupt 0x19.  This will attempt to boot from drive A and then C once.  This is useful for PC BIOSes that would only probe floppy drives at boot time.  Some other BIOSes (i. e. XT-IDE Universal BIOS) may in turn provide their own handler that replaces this one.

ALLOW_INTS:  Restores interrupts while communicating with the CH375/6.  If disabled, you may experience clock drift during I/O operations.  There is some modest risk of errors in the event interrupt handling code fired off a new disc operation in the middle of an existing one.

DOUBLE_WIDE:  Use "word" style instructions instead of "byte" ones when communicating with the CH375/6 for a modest performance boost.  This requires that the CH375/6 is wired so that pairs of ports are routed to the same place.  The Homebrew8088 design is this way by default, and the common AliExpress cards can be modified to support this, but do not do so by default.

SHADOW:  Copy the BIOS ROM into the top 6k of conventional memory, and execute from there.  Potentially a speed improvement on machines with wider or faster RAM busses than ROM (8086 or higher).

COMMAND_PORT and DATA_PORT:  Configure exactly where in the address space the CH375/6 device is.

    Homebrew8088 designs:  COMMAND_PORT = 0xE4, DATA_PORT = 0xE0

    AliExpress CH375 Card:  COMMAND_PORT = 0x261, DATA_PORT = 0x260  (subject to change based on jumpers)

    Modified for DOUBLE_WIDE AliExpress CH375 Card:  COMMAND_PORT = 0x262, DATA_PORT = 0x260 (subject to change based on jumpers)

COMMAND_PORT_2 and DATA_PORT_2:  Same, for a second device.  Leave as zero if there's only one installed.

WAIT_LEVEL:  how long to pause for the device to respond during some of the initial bring-up steps.  Unit is roughly 1/19 second.


At the very bottom of the code, there's two tables labelled DISK_1_TABLE and DISK_2_TABLE.  These are the geometries reported to software.  For normal scenarios (larger than 504Mb drives), these are sane and represent the maximum geometry described above.  If you're using a smaller drive, you may get a message at boot like

 [DANGER] Device smaller than ROM parameter table.  Suggested cylinder count: 0x0020

 In that case, replace the reference to MAX_CYL with the suggested value and reassemble.  The BIOS will advertise a geometry that actually resembles your drive.  Alternatively, go to a trade show and find a booth with people giving away bigger-than-512Mb flash drives.

## Building
The image is built with NASM.  Run

/path/to/nasm disc.asm

to get a file named "disc".

This needs to be padded to size.  You can use "padbin" [found here](http://little-scale.blogspot.com/2009/06/how-to-pad-bin-file.html)

/path/to/padbin 6144 disc

The file should then be 6144 bytes long. 

You then need a utility like "romcksum32" [found here](https://github.com/agroza/romcksum) to add a checksum.  For example

/path/to/romcksum32 -o disc

This should report "Option ROM: YES" and "ROM Blocks: 12, 6144 (6 KiB), CORRECT".

If you don't want to build it manually, a canned image is provided with the filename "ch37x-bios.bin".  This is designed around a single CH37x device, with the Homebrew8088 ports and mini-bootloader enabled.


You can now install this image whereever you want-- in a ROM on a CH375 card, or as part of a bigger 32 or 64k image that contains other options and/or the system BIOS.

It's suggested that this get loaded before other option ROMs that might touch interrupt 0x13 or 0x19, such as the XT-IDE Universal BIOS.

## Disc Image

The file "discimage.zip" expands to a 504Mb image which is partitioned as a single FAT16 partition.  You can 1) write this to a USB stick with a tool like Rufus or 2) mount it as a "raw" disc image in something like QEMU, and then do further prep-work, like installing your preferred OS.  This should hopefully make it easier to build an image with a specific CHS geometry.

## Authors and acknowledgment
Some of the initial code derives from Elijah Miller's BIOS for the Homebrew8088 boards.

## License
See LICENSE file.
